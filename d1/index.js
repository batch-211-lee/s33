//console.log("Hello World");

//Javascript Synchronous and Asynchronous

/*
	Synchronous
	-code runs in sequence. This means that each operation must wait for the previous one to complete before executing.

	Asynchronous
	-codes that run parallel. this mean that operation can occur while another one is still being processed.

	Asynchronous is often preferrable in situations where execution can be blocked. some examples of this are network requests, long-running calculations, file system operations, etc. using asynchronous code in the browser ensure the page remains response and the user experience is mostly unaffected.
*/

console.log("Hello World");
// cosole.log("Hello Again")
// for (let i = 0; i <= 1000; i ++) {
//	console.log(i)
// }

console.log("It's me again");

/*
	API stands for Application Programming Interface
	- an application programming interface is a particular set of codes that allows software programs to communicate with each other
	- an API is the interface through which you access someone else's code or through which some one else's code accesses yours

	Example:
		Google APIs
			https://developers.google.com/identity/sign-in/web/sign-in?hl=en-GB
		Youtube APIs
			https://developers.google.com/youtube/iframe_api_reference
*/

// Fetch API

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));
/*
	A "promise" is an object that represents the eventual completion, or failure, of an asynchronous and it's resulting value

	a promise is in one of these three states:
	Pending:
		initial state, neither fulfilled nor rejected
	Fulfilled:
		operation was succesffully completed
	Rejected:
		operation failed
*/

/*
	By using the .then method, we can now check for the status of the promise
	the "fetch" method will return a "promise" that resolves to be a "response" object
	-the ".then" method captures the "response" object and returns another "promise" which will eventually be "resolved" or "rejected"

	Syntax:
		fetc("URL").then (response => {})
*/

fetch("https://jsonplaceholder.typicode.com/posts").then(response => console.log(response.status));

fetch("https://jsonplaceholder.typicode.com/posts")
// use the "JSON" method from the "response" object to convert the data retrieved into JSON format to be used in our application
.then((response) => response.json())
//Print the converted JSON value from "fetch" request
//Using multiple ".then" method to create a promise chain
.then((json) => console.log(json))

/*
	"async" and "await" keywords is another approach that can be used to achieve asynchronous codes
	-used in functions to indicate which portion of code should be waited
	-creata an asynchronous Function
*/

async function fetchData () {
	//waits for the "fetch" method to complete then stores the value in the "result" variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts")
	//Result returned by fetch is returned as a promise
	console.log(result)
	//the returned "response" is an object
	console.log(typeof result)
	//We cannot access the content of the "response" by directly accessing it's body property
	console.log(result.body)
	let json = await result.json();
	console.log(json)
};

fetchData();

//Getting a specific post
//Retrieve a specific post following the Rest API (retrieve, /post/id:, GET)

fetch("https://jsonplaceholder.typicode.com/posts/1")
.then((response) => response.json())
.then((json) => console.log(json));

/*
	Postman

	url: https://jsonplaceholder.typicode.com/posts/1
	method: GET
*/

/*
	Creating a Post

	Syntax:
		fetch ("URL", options)
		.then((response) => {})
		.then ((response) => {})
*/

//creating a new post following the REST API (create, /post/:id, POST)

fetch("https://jsonplaceholder.typicode.com/posts", {
	method : "Post",
	headers : {
		"content-Type" : "application/json"
	},
	body: JSON.stringify({
		"title": "New post",
		"body": "Hello World",
		"userID": 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

/*
	Postman
	url: https://jsonplaceholder.typicode.com/posts
	method: POST
	body: raw + JSON, then:
		{
			"title" : "title value",
			"body" : "content",
			"userID" : 1
		}
*/

//Updating a post
// updates a specific post following the REST API (update, /post/:id, PUT)

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers : {
		"Content-Type" : "Application/json"
	},
	body: JSON.stringify({
		id: 1,
		title: "update post",
		body : "Hello again!",
		userID: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

/*
	POSTMAN:
		url: https://jsonplaceholder.typicode.com/posts/1
		method: PUT
		body: raw + JSON, then:
			{
				"title" : "title value",
				"body" : "revised content",
				"userID" : "value"
			}
*/

//Update a post using PATCH method

/*
	Updates a specific post following the Rest API (update, /posts/:id, Patch)
	the difference between Put and Patch is the number of properties being changed
	-PUT is used to update the whole object
	-PATCH is used to update a single/several properties
*/

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method : "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body : JSON.stringify({
		title: "corrected post"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

/*
	Postman
		url : https://jsonplaceholder.typicode.com/posts/1
		method: PATCH
		body: raw+JSON, then:
		{
			"title" : "this is my final title"
		}
*/

//Deleting a post
//Deleting a specific post following the Rest API (delete, /post/:id, DELETE)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
})

/*
	Postman
	url: https://jsonplaceholder.typicode.com/posts/1
	method: Delete
*/

//Filtering the post
/*
	the data can be filtered by sending the userID along with the url
	Information sent via URL can be done by adding the question mark symbol (?)

	Syntax:
		individual parameter:
			"url?parameterName=value"
		Multiple parameter:
			"url?ParamA=valueA&paramB=valueB"
*/
fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/posts?userId=1&userId=2&userId=3")
.then((response) => response.json())
.then((json) => console.log(json));

//Retrieve comments of a specific post
//Retreiving comments for a specific post following the Rest API (retrieve, /posts/id:, GET)

fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then((response) => response.json())
.then((json) => console.log(json))