fetch("http://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(json));

fetch("http://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => {
	let list = json.map((todo) =>{
		return todo.title;
	})

	console.log(list)
});

fetch("http://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(`the item "${json.title}" pn the list has a status of ${json.competed}.`));

fetch("http://jsonplaceholder.typicode.com/todos/1", {
	method: "POST",
	headers: {
		"Content-Type" : "application/json"
	},
	body : JSON.stringify({
		title: "Created To Do List Item",
		completed: false,
		userID: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch("http://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body : JSON.stringify({
		title : "Updated To Do List Item",
		description : "To update my to do list with a different data structure",
		status: "Pending",
		dateCompleted : "Pending",
		userID: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch("http://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body : JSON.stringify({
		status: "Completed",
		dateCompleted : " 07/09/21"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch("http://jsonplaceholder.typicode.com/todos/1", {
	method : "DELETE"
});